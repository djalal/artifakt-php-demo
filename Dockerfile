FROM registry.artifakt.io/php:8-apache

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer
COPY --chown=www-data:www-data . /var/www/html/
RUN [ -f composer.lock ] && composer install --no-cache --optimize-autoloader --no-interaction --no-ansi --no-dev || true
RUN rm -rf .git assets /root/.composer /tmp/* && chown -R www-data:www-data /var/www/html/
# copy the artifakt folder on root
RUN  if [ -d .artifakt ]; then cp -rp /var/www/html/.artifakt /.artifakt/; elif [ -d artifakt ]; then cp -rp /var/www/html/artifakt /.artifakt; fi
# run custom scripts build.sh or install.sh
RUN  if [ -f /.artifakt/build.sh ]; then /.artifakt/build.sh; fi
RUN  if [ -f /.artifakt/install.sh ]; then /.artifakt/install.sh; fi